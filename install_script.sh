#!/bin/bash
# beMaker install script
# this script needs to be ran as a superuser
echo 'installing software from script, find out more at gitlab.com/bemaker/laptop-softare-deploy/'
apt install -y --force-yes git
git clone https://gitlab.com/bemaker/laptop-software-deploy.git 
cd laptop-software-deploy/
pwd
apt update -y
apt install -y kate git inkscape gimp librecad arduino
echo 'installed kate git inkscape gimp librecad aduino from apt'
mkdir /etc/beMakerApps
cp -R binaries/ /etc/beMakerApps/
ls /etc/beMakerApps
chmod +x /etc/beMakerApps/binaries/Repetier-Host-x86_64-2.0.5.AppImage
chmod +x /etc/beMakerApps/binaries/lw.comm-server-4.0.127-x86_64.AppImage
dpkg - i /etc/beMakerApps/binaries/mBlock_4.0.4_amd64.deb
cp /etc/beMakerApps/binaries/*.AppImage ~/Desktop/
echo 'downloaded Repetier, Lazerzeb and mBlock from binaries, installed mblock with dpkg and copied to LW and repetier to Desktop as they are self contained appimages'
echo 'install done'